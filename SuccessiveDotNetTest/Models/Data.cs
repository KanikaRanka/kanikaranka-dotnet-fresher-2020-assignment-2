﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuccessiveDotNetTest.Models
{
    public class Data
    {
        public int Id { get; set; }
        [Required]
        [StringLength (50, MinimumLength = 2)]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "Only Alphabets are Required")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "Only Alphabets are Required")]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DisplayName("DateOfBirth")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = " {0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime DateOfBirth { get; set; }
        [Required]
        public int Experience { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public Nullable<int> Mobile { get; set; }
        [Required]
        [DisplayName("Date")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = " {0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime ScheduleDate { get; set; }
        [Required]
        [DisplayName("TimeFrom")]
        [DataType(DataType.Time), DisplayFormat(ApplyFormatInEditMode =true)]
        public System.TimeSpan TimeFrom { get; set; }
        [Required]
        [DisplayName("TimeTo")]
        [DataType(DataType.Time), DisplayFormat(ApplyFormatInEditMode = true)]
        public Nullable<System.TimeSpan> TimeTo { get; set; }
        [Required]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "Only Alphabets are Required")]
        public string InterviewerName { get; set; }

        public bool newUser { get; set; }

        public List<Candidate> candidatesDetails { get; set; } 


    }
}