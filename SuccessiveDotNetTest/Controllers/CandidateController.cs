﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SuccessiveDotNetTest.Models;

namespace SuccessiveDotNetTest.Controllers
{
    public class CandidateController : Controller
    {
        InterviewScheduleEntities1 interview = new InterviewScheduleEntities1();
        // GET: Candidate
        public ActionResult InterviewDetails()
        {
            var data = interview.Candidates.ToList();
            return View(data);
        }
        public ActionResult Index()
        {
            var data = interview.Candidates.ToList();
            return View(data);
            
        }

        public ActionResult Create()
        {
            var MyDetails = interview.Candidates.ToList();
            SelectList list = new SelectList(MyDetails, "Id", "FirstName","Email");
            ViewBag.userDetails = list;
            return View();
        }
        //[HttpPost]
        //public ActionResult Create(bool hasChecked)
        //{
        //    ViewBag.HasChecked = hasChecked;
        //    return View();
        //}
        [HttpPost]
        public ActionResult Create(Data model)
        {
            Candidate candidateObj = new Candidate();
            InterviewScheduleBooking interviewScheduleObj = new InterviewScheduleBooking();

            candidateObj.FirstName = model.FirstName;
            candidateObj.LastName = model.LastName;
            candidateObj.Email = model.Email;
            candidateObj.DateOfBirth = model.DateOfBirth;
            candidateObj.Mobile = model.Mobile;
            candidateObj.Experience = model.Experience;
            interview.Candidates.Add(candidateObj);
            interview.SaveChanges();
            int id = candidateObj.Id;
            interviewScheduleObj.CandidateId = id;
            interviewScheduleObj.ScheduleDate = model.ScheduleDate;
            interviewScheduleObj.TimeFrom = model.TimeFrom;
            interviewScheduleObj.TimeTo = model.TimeTo;
            interviewScheduleObj.InterviewerName = model.InterviewerName;
            interview.InterviewScheduleBookings.Add(interviewScheduleObj);
            interview.SaveChanges();
         
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            Candidate details = interview.Candidates.Find(id);
            if (details == null)
            {
                return HttpNotFound();
            }
            return View(details);

        }
        [HttpPost]
        public ActionResult Edit(Candidate details)
        {
            if (ModelState.IsValid)
            {
                interview.Entry(details).State = EntityState.Modified;
                interview.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(details);
        }
        public ActionResult ScheduleInterview()
        {
            return View();
        }
        
    }
}